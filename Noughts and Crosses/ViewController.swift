//
//  ViewController.swift
//  Noughts and Crosses
//
//  Created by Juan Ramirez on 3/21/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var gameOverLabel: UILabel!
    
    var activePlayer = 1 // 1 = noughts , 2 = crosses
    
    var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0] // 0 = empty, 1= nought, 2=cross
    
    let winningCombinations = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]]
    
    var gameActive = true
 
    
    @IBOutlet weak var playAgainButton: UIButton!
    @IBOutlet weak var button: UIButton!
    
    @IBAction func buttonPressed(sender: AnyObject) {
        
        if (gameState[sender.tag] == 0 && gameActive == true){
            
            gameOverLabel.hidden = true
            playAgainButton.hidden = true
            
            gameState[sender.tag] = activePlayer // Current active player
        
            if activePlayer == 1 {
                // Use "sender" to refer to all the buttons and link each button to this method
                sender.setImage(UIImage(named: "nought.png"), forState: .Normal)
                
                checkForSolution()
                
                //Set turn for other player
                activePlayer = 2
            }
            else{
                sender.setImage(UIImage(named: "cross.png"), forState: .Normal)
                
                checkForSolution()
                
                activePlayer = 1
            }
            
        }
    }
    
    @IBAction func playAgain(sender: AnyObject) {
        
        activePlayer = 1
        gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        gameActive = true
        
        //gameOverLabel.center = CGPointMake(self.gameOverLabel.center.x , self.gameOverLabel.center.y)
        //playAgainButton.center = CGPointMake(self.playAgainButton.center.x , self.playAgainButton.center.y)
        print("current pixel position, X: \(gameOverLabel.center.x); Y: \(gameOverLabel.center.y)")
        
        
        UIView.animateWithDuration(0.5, animations: {() -> Void in
            self.gameOverLabel.center = CGPointMake(self.gameOverLabel.center.x - 500, self.gameOverLabel.center.y)
        })
        /*UIView.animateWithDuration(0.5 { () -> Void in
            self.gameOverLabel.center = CGPointMake(self.gameOverLabel.center.x - 500, self.gameOverLabel.center.y)
        }*/
        
        UIView.animateWithDuration(1.0, animations: { () -> Void in
            self.playAgainButton.center = CGPointMake(self.playAgainButton.center.x - 500, self.playAgainButton.center.y)
        })
        
        /*UIView.animateWithDuration(1.0) { () -> Void in
            self.playAgainButton.center = CGPointMake(self.playAgainButton.center.x - 500, self.playAgainButton.center.y)
        }*/
        
        print("current pixel position, X: \(gameOverLabel.center.x); Y: \(gameOverLabel.center.y)")
        print("current pixel position, X: \(playAgainButton.center.x); Y: \(playAgainButton.center.y)")
        
        //gameOverLabel.hidden = true
        //playAgainButton.hidden = true
        
        
        var buttonToClear : UIButton
        
        for var i = 0; i < 9; i++ {
            buttonToClear = view.viewWithTag(i) as! UIButton
            
            buttonToClear.setImage(nil, forState: .Normal)
        }
        
        //viewDidLoad()
        print("About to be done")
        print("End pixel position, X: \(gameOverLabel.center.x); Y: \(gameOverLabel.center.y)")
        
        
        
    }

    
    func checkForSolution(){
        for combination in winningCombinations{
            
            if(gameState[combination[0]] == activePlayer && gameState[combination[1]] == activePlayer && gameState[combination[2]] == activePlayer) {
                
                gameActive = false
                
                if activePlayer == 1 {
                    gameOverLabel.text = "Noughts have won!"
                }
                else{
                    gameOverLabel.text = "Crosses have won!"
                }
                
                endGame()
                
            }
        }
        
        if gameActive == true {
            gameActive = false
            
            for buttonState in gameState {
                if buttonState == 0 {   //Check if there are still empty moves
                    gameActive = true
                }
            }
            
            if gameActive == false {
                gameOverLabel.text = "It's a draw!"
                endGame()
            }
        }
    }
    
    func endGame(){
        //To show the label
        gameOverLabel.hidden = false
        playAgainButton.hidden = false
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.gameOverLabel.center = CGPointMake(self.gameOverLabel.center.x + 500, self.gameOverLabel.center.y)
            
        })
        
        UIView.animateWithDuration(1.5, animations: { () -> Void in
            self.playAgainButton.center = CGPointMake( self.playAgainButton.center.x + 500, self.playAgainButton.center.y)
        })

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameOverLabel.hidden = true
        
        gameOverLabel.center = CGPointMake(gameOverLabel.center.x-500, gameOverLabel.center.y)
        
        playAgainButton.hidden = true
        
        playAgainButton.center = CGPointMake(playAgainButton.center.x - 500, playAgainButton.center.y)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

